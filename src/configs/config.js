import axios from "axios";

const API_KEY_ZOMATO = "c56f6b2bc7c3bdf6de5532da01661a1b";
const axiosCustom = axios.create({
  baseURL: "https://developers.zomato.com/api/v2.1/",
  timeout: 5000,
  headers: { "user-key": API_KEY_ZOMATO }
});

export default axiosCustom;
